import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Product } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  getProducts() {
    return this.firestore.collection('Products').snapshotChanges();
  }

  getProduct(id) {
    return this.firestore.collection('Products').doc(id).valueChanges();
  } 

  deleteProduct(id) {
    this.firestore.doc('Products/'+id).delete();
  }
 
  createProduct(product: Product){
    return this.firestore.collection('Products').add(product);
  }
 
  updateProduct(id, product: Product) {
    this.firestore.collection('Products').doc(id).update(product)
      .then(() => {
        this.router.navigate(['adm/product-list']);
      }).catch(error => console.log(error));
  }
}
