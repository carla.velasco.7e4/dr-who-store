import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { User } from '../interfaces/interface';


@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  userData: any;

  constructor(public afStore: AngularFirestore,
   public ngFireAuth: AngularFireAuth, public router: Router, private firestore: AngularFirestore) {
     this.ngFireAuth.authState.subscribe(user => {
       if (user) {
         this.userData = user;
         localStorage.setItem('user', JSON.stringify(this.userData));
       } else {
         localStorage.setItem('user', null);
       }
     })
 }

  signIn(email, password) {
    console.log('signIn()');
    return this.ngFireAuth.signInWithEmailAndPassword(email, password)
  }

  registerUser(userName, email, password, isAdmin, lat, long) {
    console.log('registerUser()');
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      var loginClient = JSON.parse(localStorage.getItem('loginClient'));
      localStorage.removeItem('loginClient');
      const  userData: User = {
        uid: email,
        email: email,
        displayName: userName,
        isAdm: isAdmin,
        lat: lat,
        lon: long,
      }
      if(!loginClient) {
         return this.setUserData(userData);
      }
      localStorage.setItem('userData', JSON.stringify(userData));
    });
  }

  registerCoord(email) {
    var user = this.getUserById(email);
    console.log(user);

  }

  setUserData(user) {
    console.log('setUserData()');
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      isAdm: user.isAdm,
      lat: user.lat,
      lon: user.lon
    }
    return userRef.set(userData)
  }

  get isLoggedIn(): boolean {
    console.log('isLoggedIn()');
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    return (user !== null) ? true : false;
  }

  get isAdm(): boolean {
    console.log('isAdm()');
    const isAdm = JSON.parse(localStorage.getItem('isAdm'));
    return isAdm;
  }

  getUserById(email) {
    console.log('getUserById()');
    return this.firestore.collection('users').doc(email).valueChanges();
  }

  signOut() {
    console.log('signOut()');
    return this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      localStorage.removeItem('isAdm');
      this.router.navigate(['login']);
    })
  }

  
}
