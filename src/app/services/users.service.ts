import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private firestore: AngularFirestore, private router: Router) { }

  getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }

  getUser(id) {
    return this.firestore.collection('users').doc(id).valueChanges();
  } 

  deleteUser(id) {
    this.firestore.doc('users/'+id).delete();
  }
 
  createUser(user: User){
    return this.firestore.collection('users').add(user);
  }
 
  updateUser(id, user: User) {
    this.firestore.collection('users').doc(id).update(user)
      .then(() => {
        this.router.navigate(['adm/client-list']);
      }).catch(error => console.log(error));
  }
}
