import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'adm/create-product',
    loadChildren: () => import('./pages/adm/create-product/create-product.module').then( m => m.CreateProductPageModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'adm/product-list',
    loadChildren: () => import('./pages/adm/product-list/product-list.module').then( m => m.ProductListPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'adm/edit-product/:id',
    loadChildren: () => import('./pages/adm/edit-product/edit-product.module').then( m => m.EditProductPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'client/product-list',
    loadChildren: () => import('./pages/client-product-list/client-product-list.module').then( m => m.ClientProductListPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'adm/client-map/:uid',
    loadChildren: () => import('./pages/adm/client-map/client-map.module').then( m => m.ClientMapPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'adm/client-list',
    loadChildren: () => import('./pages/adm/client-list/client-list.module').then( m => m.ClientListPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'adm/choose-action',
    loadChildren: () => import('./pages/adm/choose-action/choose-action.module').then( m => m.ChooseActionPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'adm/create-user',
    loadChildren: () => import('./pages/adm/create-user/create-user.module').then( m => m.CreateUserPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'send-email',
    loadChildren: () => import('./pages/send-email/send-email.module').then( m => m.SendEmailPageModule)
  },
  {
    path: 'register-map',
    loadChildren: () => import('./pages/register-map/register-map.module').then( m => m.RegisterMapPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
