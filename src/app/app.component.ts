import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

import { AuthenticationService } from './services/authentication.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {

  isLoggedIn;
  isAdm;

  constructor(private authService: AuthenticationService, private menu: MenuController) {}

  ngOnInit() {
      this.isAdm = this.authService.isAdm;
      this.isLoggedIn = this.authService.isLoggedIn;
  }

  async checkLogged() {
    console.log('checkLogged');

    this.isAdm = await this.authService.isAdm;
    this.isLoggedIn = await this.authService.isLoggedIn;
  }

  openFirst() {
    this.checkLogged().then(() => {
      console.log('openFirst');
      this.menu.enable(true, 'first');
      this.menu.open('first');
      location.reload();
    });
  }

  openEnd() {
    console.log('openEnd');

    this.menu.open('end');
    this.checkLogged();
  }

  openCustom() {
    console.log('openCustom');

    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  } 

  logOut() {
    this.authService.signOut();
    this.checkLogged();
  }
}
