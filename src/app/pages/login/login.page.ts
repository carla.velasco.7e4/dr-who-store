import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  loginForm: FormGroup
  user: any

  constructor(private authService: AuthenticationService,
    public formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: [''],
    })
  }

  onSubmit() {
    if (!this.loginForm.valid) {
      return false;
    } else {
      this.logIn(this.loginForm.value.email, this.loginForm.value.password);
    }
  }

  redirect(adm) {
    (adm) ? this.router.navigateByUrl('/adm/choose-action') : this.router.navigateByUrl('/client/product-list');
  }

  logIn(email, password) {
    this.authService.signIn(email, password)
      .then(
        (res) => {
        this.authService.getUserById(email).subscribe(
          res => {
            localStorage.setItem('isAdm', res['isAdm'].toString());
            this.redirect(res['isAdm']);
          })
      }).catch((error) => {
        window.alert(error.message)
      })
  }

}
