import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup

  constructor(private authService: AuthenticationService,
    public formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      userName: [''],
      email: [''],
      password: [''],
      isAdm: [false],
      lat: [''],
      long: ['']
    })
  }

  onSubmit() {
    if (!this.registerForm.valid) {
      return false;
    } else {
      localStorage.setItem('loginClient', JSON.stringify(true));
      this.signUp(this.registerForm.value.userName,
        this.registerForm.value.email,
         this.registerForm.value.password, 0, 0);
    }
  }

  signUp(userName, email, password, lat, long){
    this.authService.registerUser(userName, email, password, false, lat, long)     
    .then((res) => {
      this.authService.signIn(email, password);
      this.router.navigate(['register-map']);
    }).catch((error) => {
      window.alert(error.message)
    })
  }
}
