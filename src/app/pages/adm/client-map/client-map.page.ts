import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UsersService } from '../../../services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var mapboxgl: any;

@Component({
  selector: 'app-client-map',
  templateUrl: './client-map.page.html',
  styleUrls: ['./client-map.page.scss'],
})

export class ClientMapPage implements OnInit {

  long;
  lat;
  id;
  user;

  constructor(private data: UsersService, private activatedRoute: ActivatedRoute) {

    this.id = this.activatedRoute.snapshot.paramMap.get('uid');
    this.data.getUser(this.id).subscribe(
      res => {
        this.user = res;
        this.lat = this.user.lat;
        this.long = this.user.lon;
        this.user = this.user.uid;

        this.mostrarMapa();
      }
    );
   }

  ngOnInit() {}

  mostrarMapa() {
    mapboxgl.accessToken = environment.mapBoxToken;
    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [this.long, this.lat],
      zoom: 14
    });

    map.on('load', () => {
      map.resize();
    });

    map.addControl(new mapboxgl.NavigationControl());

    var marker = new mapboxgl.Marker({})
    .setLngLat([this.long, this.lat])
    .setPopup(new mapboxgl.Popup().setHTML("<h5 style='color: black; padding: 5px; padding-left: 15px;padding-right: 10px;'>" + this.user + "</h5>"))
    .addTo(map);
  }
}




