import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClientMapPageRoutingModule } from './client-map-routing.module';

import { ClientMapPage } from './client-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientMapPageRoutingModule
  ],
  declarations: [ClientMapPage]
})
export class ClientMapPageModule {}
