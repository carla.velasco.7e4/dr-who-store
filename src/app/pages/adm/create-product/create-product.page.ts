import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FirestorageService } from '../../../services/firestorage.service';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.page.html',
  styleUrls: ['./create-product.page.scss'],
})
export class CreateProductPage implements OnInit {

  createProduct: FormGroup;

  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,
    private router: Router,
    public firestorageService: FirestorageService ) { }

  ngOnInit() {
    this.createProduct = this.formBuilder.group({
      category: [''],
      name: [''],
      price: [],
    })
  }

  onSubmit() {
    if (!this.createProduct.valid) {
      return false;
    } else {
      this.dataService.createProduct(this.createProduct.value)
      .then(() => {
        this.createProduct.reset();
        this.router.navigate(['adm/product-list']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }

  async newImageUpload(event: any) {
    const path = 'Productos';
    const name = 'prueba';
    const file = event.target.files[0];
    
    const res = await this.firestorageService.uploadImage(file, path, name);
    console.log('recibí res de la promesa ', res);
    console.log('fin de la función ->newImageUpload');
  }

}
