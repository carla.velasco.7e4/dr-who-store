import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Product } from '../../../interfaces/interface';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
})

export class ProductListPage implements OnInit {

  products = [];

  constructor(private data: DataService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    this.data.getProducts().subscribe(
      res=>{
        this.products = res.map((item) => {
          return {
            id: item.payload.doc.id, ... item.payload.doc.data() as Product
          }
        })
      }
    )
  }

  deleteProduct(id){
    if (window.confirm('¿Quieres eliminar este producto?')) {
      this.data.deleteProduct(id)
    }
  }
}
