import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { User } from '../../../interfaces/interface';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.page.html',
  styleUrls: ['./client-list.page.scss'],
})
export class ClientListPage implements OnInit {

  users = [];

  constructor(private data: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.data.getUsers().subscribe(
      res=>{
        this.users = res.map((item) => {
          return {
            id: item.payload.doc.id, ... item.payload.doc.data() as any
          }
        })
      }
    )
  }

}
