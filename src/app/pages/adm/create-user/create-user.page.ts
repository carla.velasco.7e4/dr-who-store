import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { environment } from 'src/environments/environment';
import {MapboxService, Feature } from '../../../services/mapbox.service';
declare var mapboxgl: any;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.page.html',
  styleUrls: ['./create-user.page.scss'],
})

export class CreateUserPage implements OnInit {

  registerForm: FormGroup;
  addresses: string[] = [];
  selectedAddress = null;
  long;
  lat;
  marker;
  map;

  constructor(private authService: AuthenticationService,
    public formBuilder: FormBuilder,
    private router: Router,
    private mapboxService: MapboxService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      userName: [''],
      email: [''],
      password: [''],
      isAdm: [true]
    })

  }

  onSubmit() {

    if (!this.registerForm.valid) {
      return false;
    } else {
      this.signUp(this.registerForm.value.userName,
        this.registerForm.value.email,
         this.registerForm.value.password,
         this.registerForm.value.isAdm, 0, 0);
    }
  }

  search(event: any) {
    const searchTerm = event.target.value.toLowerCase();
    if(searchTerm && searchTerm.length > 0) {
      this.mapboxService
        .search_word(searchTerm)
        .subscribe((features: Feature[]) => {
          this.addresses = features.map(feat => feat.place_name)
          this.long = features[0].center[0];
          this.lat =  features[0].center[1];
          this.marker.setLngLat([this.long, this.lat]).addTo(this.map)
          this.map.flyTo({
            center: [this.long, this.lat]
            });
        });
      } else {
        this.addresses = [];
      }
    }


  signUp(userName, email, password, isAdm, lat, lon){
    this.authService.registerUser(userName, email, password, isAdm, lat, lon)     
    .then((res) => {
      this.authService.signIn(email, password);
      this.router.navigate(['adm/choose-action']);
    }).catch((error) => {
      window.alert(error.message)
    })
  }
}
