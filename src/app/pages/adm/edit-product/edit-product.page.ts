import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.page.html',
  styleUrls: ['./edit-product.page.scss'],
})

export class EditProductPage implements OnInit {

  editProduct: FormGroup;
  id: String;
  
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router, public formBuilder: FormBuilder) {
      
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getProduct(this.id).subscribe(
      res => {
        this.editProduct = this.formBuilder.group({
          name: [res['name']],
          category: [res['category']],
          price: [res['price']]
        })
      });
  }

  ngOnInit(): void {
    this.editProduct = this.formBuilder.group({
      category: [''],
      name: [''],
      price: []
    }) 
  }

  onSubmit() {
    this.dataService.updateProduct(this.id, this.editProduct.value);
    this.router.navigate(['adm/product-list']);
  } 
}
