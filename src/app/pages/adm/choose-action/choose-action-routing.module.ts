import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseActionPage } from './choose-action.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseActionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseActionPageRoutingModule {}
