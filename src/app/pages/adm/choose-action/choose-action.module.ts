import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseActionPageRoutingModule } from './choose-action-routing.module';

import { ChooseActionPage } from './choose-action.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseActionPageRoutingModule
  ],
  declarations: [ChooseActionPage]
})
export class ChooseActionPageModule {}
