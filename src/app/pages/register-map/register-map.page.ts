import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import {MapboxService, Feature } from '../../services/mapbox.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';


declare var mapboxgl: any;

@Component({
  selector: 'app-register-map',
  templateUrl: './register-map.page.html',
  styleUrls: ['./register-map.page.scss'],
})
export class RegisterMapPage implements OnInit {

  addresses: string[] = [];
  selectedAddress = null;
  long;
  lat;
  marker;
  map;
  userData;

  constructor(private mapboxService: MapboxService, private authService: AuthenticationService, public formBuilder: FormBuilder,
    private router: Router) {
    this.userData = JSON.parse(localStorage.getItem('userData'));
    console.log(this.userData);
  }

  saveCoordenades() {
    this.userData.lat = this.lat;
    this.userData.lon = this.long;
    this.authService.setUserData(this.userData).then(
      res => {
        this.router.navigate(['client/product-list']);
      }
    )
   
  }

  
  search(event: any) {
    const searchTerm = event.target.value.toLowerCase();
    if(searchTerm && searchTerm.length > 0) {
      this.mapboxService
        .search_word(searchTerm)
        .subscribe((features: Feature[]) => {
          this.addresses = features.map(feat => feat.place_name)
          this.long = features[0].center[0];
          this.lat =  features[0].center[1];
          this.marker.setLngLat([this.long, this.lat]).addTo(this.map)
          this.map.flyTo({
            center: [this.long, this.lat]
            });
        });
      } else {
        this.addresses = [];
      }
    }
  

  onSelect(address: string) {
    this.selectedAddress = address;
    this.addresses = [];
  }

  ngOnInit() {
    mapboxgl.accessToken = environment.mapBoxToken;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [2.1854994, 41.4532659],
      zoom: 14
    });

    this.map.on('load', () => {
      this.map.resize();
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    this.marker = new mapboxgl.Marker({})
    .setLngLat([2.171332648, 41.402165058])
    .setPopup(new mapboxgl.Popup().setHTML("<h1>Títol del marcador</h1>"))
    .addTo(this.map);
  }


}
