import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterMapPage } from './register-map.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterMapPageRoutingModule {}
