import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterMapPageRoutingModule } from './register-map-routing.module';

import { RegisterMapPage } from './register-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RegisterMapPageRoutingModule
  ],
  declarations: [RegisterMapPage]
})
export class RegisterMapPageModule {}
