import { Component, OnInit } from '@angular/core';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { DataService } from './../../services/data.service';
import { Router } from '@angular/router';
import { Product } from './../../interfaces/interface';



@Component({
  selector: 'app-send-email',
  templateUrl: './send-email.page.html',
  styleUrls: ['./send-email.page.scss'],
})

export class SendEmailPage {

  products = [];
  correu;

  ngOnInit() {
    this.correu = localStorage.getItem('email');
    localStorage.removeItem('email');
    this.sendEmail();
  }

  ionViewWillEnter() {
    this.correu = localStorage.getItem('email');
    localStorage.removeItem('email');
    this.sendEmail();
  }

  getProducts(){
    this.data.getProducts().subscribe(
      res=>{
        this.products = res.map((item) => {
          return {
            id: item.payload.doc.id, ... item.payload.doc.data() as Product
          }
        })
      }
    )
  }

  constructor(private emailComposer: EmailComposer, private router: Router, public data: DataService) {}

  sendEmail(){
    let email = {
      to: this.correu, bcc: ['john@doe.com', 'jane@doe.com'],
      subject: 'Compra realitzada',
      body: 'La compra s\'ha realitzat correctament.', isHtml: true
    }
    this.emailComposer.open(email);
    this.router.navigate(['client/product-list']);
  }

}
