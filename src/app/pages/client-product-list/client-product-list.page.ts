import { Component, OnInit } from '@angular/core';
import { DataService } from './../../services/data.service';
import { AuthenticationService } from './../../services/authentication.service';
import { Router } from '@angular/router';
import { Product } from './../../interfaces/interface';

@Component({
  selector: 'app-client-product-list',
  templateUrl: './client-product-list.page.html',
  styleUrls: ['./client-product-list.page.scss'],
})

export class ClientProductListPage implements OnInit {

  products = [];
  compra = [];

  constructor(private router: Router, private data: DataService, private auth: AuthenticationService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    this.data.getProducts().subscribe(
      res=>{
        this.products = res.map((item) => {
          return {
            id: item.payload.doc.id, ... item.payload.doc.data() as Product
          }
        })
      }
    )
  }

  
  comprar() {
      var user = JSON.parse(localStorage.getItem('user'));
      localStorage.setItem('email', user.email);
      this.router.navigate(['send-email']);
    }
  
}
