export interface Product {
    id: string;
    name: string;
    category: string;
    price: number;
    quantity: number;
}

export interface User {
    uid: string;
    email: string;
    displayName: string;
    isAdm: boolean;
    lat: number;
    lon: number;
}